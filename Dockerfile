FROM debian:stretch-slim

COPY dlangbot /dlangbot

RUN echo ">>> Making apt non-interactive <<<"                          \
 && export DEBIAN_FRONTEND=noninteractive                              \
 && echo 'APT::Get::Assume-Yes "true";'                                \
      >> /etc/apt/apt.conf.d/99dlangbot                                \
 && echo 'DPkg::Options "--force-confnew";'                            \
      >> /etc/apt/apt.conf.d/99dlangbot                                \
 && echo ">>> Updating <<<"                                            \
 && apt-get update -qq                                                 \
 && echo ">>> Installing necessary dependencies <<<"                   \
 && apt-get install -y --no-install-recommends                         \
      ca-certificates libssl1.1 2>&1 > /dev/null                       \
 && echo ">>> Cleaning up <<<"                                         \
 && rm -rf /var/lib/apt/lists /var/cache/apt/archives                  \
 && rm -rf /etc/apt/apt.conf.d/99dlangbot                              \
 && echo ">>> Adding 'app' user <<<"                                   \
 && useradd -UM --shell /bin/false app                                 \
 && chown -R app:app /dlangbot                                         \
 && chmod -R 755 /dlangbot

ENTRYPOINT [ "/dlangbot" ]
