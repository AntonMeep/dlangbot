module run;

import vibe.data.json;
import vibe.http.client;
import vibe.core.log;

import db : Settings;

struct Runner {
	private {
		string m_url;
	}

	struct Input {
		string source;
		string compiler = "dmd";
		string stdin;
		string args;
		bool color = false;
	}

	struct Output {
		string output;
		bool success;
		struct CompilerMessage {
			int line;
			string message;
		}

		CompilerMessage[] errors;
		CompilerMessage[] warnings;
	}

	this(string url) {
		"Initializing runner using %s backend".logInfo(url);
		m_url = url;
	}

	const auto run(string source, Settings s) {
		return this.run(source, s.compiler, s.stdin, s.args);
	}

	const auto run(string source, string compiler, string stdin, string args) {
		Output r;
		(m_url ~ "/api/v1/run").requestHTTP(
			(scope req) {
				req.method = HTTPMethod.POST;
				req.writeJsonBody(Input(source, compiler, stdin, args).serializeToJson);
			},
			(scope res) {
				r = res.readJson.deserializeJson!Output;
			}
		);
		return r;
	}
}