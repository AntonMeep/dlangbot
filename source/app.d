import vibe.core.args;
import vibe.core.core;
import vibe.core.log;
import bot;

int main() {
	LogLevel.debug_.setLogLevel;
	setLogFormat(FileLogger.Format.threadTime, FileLogger.Format.threadTime);

	string botToken;
	string mongoUrl = "mongodb://localhost/";
	string runUrl = "https://run.dlang.io";

	"token|t".readOption(&botToken, "Telegram bot token");
	"mongo|m".readOption(&mongoUrl, "URL of the running mongodb");
	"run|r".readOption(&runUrl, "URL of the run API to use. By default it's https://run.dlang.io, but for performance, don't-be-a-jerk-and-run-your-own-instance, and reliability reasons it's recommended to run own fork of dlang-tour's core using docker");

	return Dlangbot(botToken, mongoUrl, runUrl).run;
}
