module speech;

import std.random;

string Speech(string category)() {
	mixin("return " ~ category ~ "_messages.choice;");
}

private:
enum start_messages = [
q"EOS
Hello there new user!
Just message me with the D code you want me to compile and /execute. Type /help to see all of the available commands
EOS",
];
enum help_messages = [
q"EOS
I'm glad to compile and execute code written in D programming language. Just message it to me!

_Note_: in groups you have to use /execute command

*Commands*
/help - show this message
/settings - show current settings
/execute - execute code

*Settings*
/compiler - use different compiler (dmd/ldc)
/dmd, /ldc - shorthand for /compiler
/stdin - set program stdin
/args - set compiler arguments

Pssst, /statistics shows some global statistics
EOS",
];
enum settings_messages = [
q"EOS
Your settings are:

Compiler: `%1$s`
Compiler arguments: `%3$s`
Stdin: `%2$s`
EOS",
];
enum unknown_command_messages = [
	"Sadly, I don't know the `%s` command. Do you need some /help?",
	"Roses are red\nViolets are blue\nIt's so unfortunate that I\nDon't quite understand you?",
];
enum cannot_edit_while_command_messages = [
	"Sorry, edited messages are ignored while an operation is going. /cancel it first",
	"Whoa, hold on! /cancel current action before editing things",
];
enum cannot_edit_command_messages = [
	"Edited commands are ignored. Check /help if you need",
];
enum empty_compiler_messages = [
	"Your compiler has been reset to dmd",
];
enum cancel_command_messages = [
	"The command has been cancelled",
];
enum compiler_set_command_messages = [
	"What compiler do you want to use? If you don't want to change it, /cancel current operation. Also, you can always use /empty to reset this setting",
];
enum compiler_set_messages = [
	"Got it, now using %s compiler",
	"%1$s is a good choice. Did you know, that %1$s is the most popular compiler amongst %1$s's users?",
	"%s will be used now",
];
enum compiler_unknown_messages = [
	"Are you sure this is a compiler? Valid choices are dmd or ldc",
];
enum stdin_set_command_messages = [
	"Setting custom stdin for your programs. Message it to me. Also, you can use /empty if you want to reset it or /cancel to cancel this command",
];
enum empty_stdin_messages = [
	"Your stdin setting has been reset. It's empty now",
	"Stdin setting has been reset to its default value of nothing",
];
enum stdin_set_messages = [
	"Sure, no problem, using `%s` as stdin for your program",
];
enum stdin_limit_messages = [
	"Oops, maximum allowed length of the stdin is %d characters",
];
enum args_set_command_messages = [
	"You can now message me with arguments for the compiler. Look for more info here: [dmd](https://dlang.org/dmd-linux.html), [ldc](https://wiki.dlang.org/Using_LDC). If you changed your mind, feel free to /cancel this operation or use /empty to remove any arguments you've set previously'",
];
enum empty_args_messages = [
	"Emptying compiler arguments... done!",
	"Sweep-sweep, compiler arguments are empty now",
];
enum args_set_messages = [
	"That's a good choice of arguments: `%s`",
	"Gotcha', using `%s` as compiler arguments",
];
enum args_limit_messages = [
	"Even though I like your writing style, maximum allowed length of this setting is just %d characters",
];
enum execute_code_messages = [
	"Great! Just message me the code and don't be afraid if you make a mistake - you can always edit it afterwards. Feel free to /cancel this operation if you changed your mind",
];
enum execute_code_cannot_empty_messages = [
	"That's not how it works. Send me the code you want me to execute or /cancel this command",
];
enum execute_code_backend_error_messages = [
	"Something went wrong, back-end returned: `%s`",
	"`%s` is what back-end said about this. What's going on?",
];
enum execute_code_error_messages = [
	"❗ Error!",
];
enum execute_code_warning_messages = [
	"👍 Successful, but there are some warnings",
];
enum execute_code_success_messages = [
	"👍 Success!",
];
enum execute_code_output_truncated_messages = [
	"Output of the program is too long and it has been truncated",
];
enum execute_code_showing_error_messages = [
	"Errors:",
];
enum code_limit_messages = [
	"I'm horribly sorry, but maximum length of code I can compile and execute is %d characters. Don't give up though, there's always https://run.dlang.io which has far more generous limits",
];
enum breaking_bad_messages = [
	"Stop right there, you just did something that wasn't expected. Could you open an issue explaining what you've done on the GitLab or message the author? Links are in my profile (click my name)",
];
enum nothing_to_cancel_or_empty_messages = [
	"There's nothing to %s",
];
enum other_audio_messages = [
	"I like it. Can we get back to, you know, _programming_? Hit /help for that",
];
enum other_document_messages = [
	"That's a nice file of yours, I guess. I am not interested though. Check out /help if you don't know what to do",
];
enum other_game_messages = [
	"I'll play that later. I'm working right now",
];
enum other_photo_messages = [
	"That's a cute picture. Is it something from Monet?",
];
enum other_sticker_messages = [
	"I think so\nWait\nIs this a sticker? A sticker in 2000 and 18?\nI'm not gonna judge you, just stop. Please",
	"Could you not?",
];
enum other_video_messages = [
	"<3. I'm here to help you with your code, but you can always send me videos like this",
	"Please, tell me there's more of this on the internet",
];
enum other_voice_messages = [
	"I wish I could hear what you said, but I don't have ears. Could you write it down?",
];
enum other_video_note_messages = [
	"Yes, please. Just wait until I finish my job and I'm up for it",
];
enum other_contact_messages = [
	"I'm not quite sure if I'd like to meet that person. Are you sure they are ok with hanging out with a bot?",
];
enum other_location_messages = [
	"Not a problem, I'll pick you up right there. Oh no, I don't have a driver's licence. I'm sorry but you'll have to catch a bus or something",
];
enum other_venue_messages = [
	"I'm a simple bot, you tell me where, I ask \"when?\". So when?",
	"I hope they serve bolts and wires. I'm starving over here",
];
enum reaction_thumbs_up_messages = [
	"You reacted 👍, it's so nice of you",
];
enum remove_reaction_thumbs_up_messages = [
	"Fair enough",
];
enum reaction_thumbs_down_messages = [
	"You reacted 👎 to this. I'm sorry",
];
enum remove_reaction_thumbs_down_messages = [
	"I knew you can't do that to me!",
];
enum wrong_callback_messages = [
	"Oops, something went wrong",
];
enum show_statistics_messages = [
q"EOS
So, you like these boring numbers? Fine, here are some statistics:

- %d chats
- %d executions of code, %d times successfully and %d with errors
- Total number of code characters is %d, this code produced %d characters of output
- Total number of errors is %d, total number of warnings is %d
EOS",
];