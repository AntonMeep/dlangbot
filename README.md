dlangbot
=============

This Telegram bot compiles and runs code written in the D programming language. Multiple compilers are available (dmd/ldc).

## 🏗️ 🚧 👷 Under construction 👷 🚧 🏗️

Code is still being worked on, any ideas regarding user interaction/code/deploy/etc are greatly appreciated.

## Screenshots
![hello](misc/dlangbot.hello.png)
![stdin](misc/dlangbot.stdin.png)
